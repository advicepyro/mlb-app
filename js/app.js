/* All logic for the MLB app. */

$(document).ready(function() {

	// Init modal
	$("#loader-modal").modal('show');

	// Init the datepicker, then set the initial item to be now
	$('#date-picker-sel').datepicker({
	    format: "D M dd yyyy",
	    orientation: "auto",
	    autoclose: true,
	    todayHighlight: true
	});
	$('#date-picker-sel').datepicker('update', new Date());

	// Buttons to go back and forth for dates
	$('#date-picker-prev').click(function() {
		var thisDate = $('#date-picker-sel').datepicker('getDate');
		thisDate.setDate(thisDate.getDate() - 1);
		$('#date-picker-sel').datepicker('setDate', thisDate);
	});
	$('#date-picker-next').click(function() {
		var thisDate = $('#date-picker-sel').datepicker('getDate');
		thisDate.setDate(thisDate.getDate() + 1);
		$('#date-picker-sel').datepicker('setDate', thisDate); 
	});

	// Invoke main function if I manually select a day 
	$('#date-picker-sel').datepicker().on('changeDate', updateData);


	// Finally, let's load today's data
	updateData();

	// Main logic goes here.
	function updateData() {

		// Show loading modal
		$("#loader-modal").modal('show');

		// Disable date picker buttons and clear away all cards
		$(".date-picker-element").attr('disabled', true);
		$("#scorecard-root").children('.scorecard').slideUp();
		$("#scorecard-root").empty();

		// First determine the URL to request..
		var thisDate = $('#date-picker-sel').datepicker('getDate');
		var dataUrl = "http://gd2.mlb.com/components/game/mlb/year_" + thisDate.getFullYear() 
			+ "/month_" + pad(thisDate.getMonth() + 1) + "/day_" + pad(thisDate.getDate()) 
			+ "/master_scoreboard.json";

		// Retrieve data...
		$.ajax({
			url: dataUrl,
			dataType: "json",
			success: function(jsondata) {

				var gameNode = jsondata.data.games.game;
				if (!gameNode) {
					
					// No games on this day. Add an alert.
					$("#scorecard-root").append('<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-ban-circle"></span> There are no games on this day.</div>');
				} else {
					
					// We have games. Let's start parsing.
					// Because we may have one game (an object) or many (an array),
					// we're splitting the add code to a helper
					if (gameNode.constructor === Array) {
						$.each(gameNode, function(key, value) {
							addElementHelper(value);
						});
					} else {
						addElementHelper(gameNode);
					}
				}
				$("#loader-modal").modal('hide');
				$(".date-picker-element").removeAttr('disabled');
			},
			error: function() {

				// Display an error just as useless as Windows 10 upgrade errors :)
				$("#scorecard-root").append('<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-ban-circle"></span> Oops! Something happened.</div>');
				$("#loader-modal").modal('hide');
				$(".date-picker-element").removeAttr('disabled');
			}
		});
	}

	function addElementHelper(item) {

		// Make a copy of the main scorecard
		var $newcard = $(".master-item").clone().hide().removeClass("master-item");

		// Now change the inner elements appropriately
		// For the title, there are some cities with more than one team (NY, Chicago)
		// In the dataset, the city name becomes "NY Mets" or "NY Yankees"
		// We also need to account for All-Star games as well..
		var homeTeam = (((item.home_team_city).indexOf(item.home_team_name) == -1) && ((item.home_team_name != "American") && (item.home_team_name != "National"))) ? item.home_team_city + " " + item.home_team_name : item.home_team_city;
		var awayTeam = (((item.away_team_city).indexOf(item.away_team_name) == -1) && ((item.home_team_name != "American") && (item.home_team_name != "National"))) ? item.away_team_city + " " + item.away_team_name : item.away_team_city;
		var description = (item.description == "") ? "" : " [" + item.description + "]";
		$newcard.find(".panel-title").html(homeTeam + " vs. " + awayTeam + description);
		$newcard.find(".home-team > .team-name").html(item.home_team_name);
		$newcard.find(".away-team > .team-name").html(item.away_team_name);
		if (item.linescore) {
			$newcard.find(".home-team > .score").html(item.linescore.r.home);
			$newcard.find(".away-team > .score").html(item.linescore.r.away);

			// Pick a team to highlight.
			// We need to explicitly state both cases to avoid highlighting ties for games-in-progress
			if (parseInt(item.linescore.r.home) > parseInt(item.linescore.r.away)) {
				$newcard.find(".home-team").addClass("success");
			} else if (parseInt(item.linescore.r.home) < parseInt(item.linescore.r.away)) {
				$newcard.find(".away-team").addClass("success");
			}

		}
		$newcard.find(".status-text").html(item.status.status);


		// If Toronto is in this, let's paint it blue and pin to top
		if ((item.home_team_city == "Toronto") || (item.away_team_city == "Toronto")) {
			$newcard.find(".panel").removeClass("panel-default").addClass("panel-primary");
			$newcard.prependTo("#scorecard-root");
		} else {
			$newcard.appendTo("#scorecard-root")
		}

		$newcard.slideDown();
	}

	// Used to add an additional 0 if less than 10.
	// Source: http://stackoverflow.com/a/12550320
	function pad(n) {
		return n < 10 ? '0' + n : n
	}
});